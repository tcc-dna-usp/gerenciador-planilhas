import { useEffect, useState } from "react";

import { SheetViewer } from "./SheetViewer";
import { useSheetGetterFromContext } from "../lib/hooks/useSheetGetterFromContext";

export const SheetViewerHoc = (props: any) => {
  //get sheet from hook useSheetFromContext
  const sheetGetter = useSheetGetterFromContext();
  // sheet state
  const [sheetState, setSheetState] = useState<any[] | undefined>(undefined);

  // useEffect to get sheet data from sheetGetter and set sheetState
  useEffect(() => {
    sheetGetter().then((sheet: any) => {
      setSheetState(sheet);
    });
  }),
    [];

  // return sheetviewer component
  return <SheetViewer sheet={sheetState} />;
};
