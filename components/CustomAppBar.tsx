import { AppBar, Stack, Toolbar, Typography } from "@mui/material";

import { Login } from "./Login";
import { Logout } from "./Logout";
import { useAuthData } from "../lib/context/AuthContext";

export const CustomAppBar = () => {
  const { isLoggedIn, authData } = useAuthData();

  return (
    <AppBar position="static">
      <Toolbar>
        <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
          G(DNA - USP)
        </Typography>
        {isLoggedIn ? (
          <Stack direction="row" alignItems="center" spacing={2}>
            <Typography component="div">
              {authData?.profile.firstName}
            </Typography>
            <Logout />
          </Stack>
        ) : (
          <Stack direction="row" alignItems="center" spacing={2}>
            <Typography component="div">Login</Typography>
            <Login />
          </Stack>
        )}
      </Toolbar>
    </AppBar>
  );
};
