import { IconButton } from "@mui/material";
import LogoutIcon from "@mui/icons-material/Logout";
import { useAuthData } from "../lib/context/AuthContext";

export const Logout = () => {
  const { logout } = useAuthData();

  return (
    <IconButton onClick={logout} sx={{ backgroundColor: "white" }}>
      <LogoutIcon />
    </IconButton>
  );
};
