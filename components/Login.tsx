import { Google } from "@mui/icons-material";
import { IconButton } from "@mui/material";
import { SheetsManager } from "../lib/SheetsManager";
import { useAuthData } from "../lib/context/AuthContext";

export const Login = () => {
  return (
    <div>
      <IconButton
        onClick={useAuthData().login}
        sx={{ backgroundColor: "white" }}
      >
        <Google />
      </IconButton>
    </div>
  );
};
