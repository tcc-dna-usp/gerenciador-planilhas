import { createContext, useContext, useEffect, useState } from "react";
import { useGoogleLogin, useGoogleLogout } from "react-google-login";

import { loginProps } from "../gapi/loginProps";
import { useRouter } from "next/dist/client/router";

export type ProfileData = {
  firstName: string;
  fullName: string;
  email: string;
  imageUrl: string;
};

export type AuthData = {
  accessToken: string;
  profile: {
    firstName: string;
    fullName: string;
    email: string;
    imageUrl: string;
  };
};

export type AuthContextData = {
  authData?: AuthData;
  isLoggedIn: boolean;
  login: () => void;
  logout: () => void;
};

export const AuthContext = createContext<AuthContextData>({
  isLoggedIn: false,
  login: () => {},
  logout: () => {},
  authData: undefined,
});

export const useAuthData = () => useContext(AuthContext);

export const AuthProvider = ({
  children,
  tryLogin,
  redirectOnFail,
  renderWithoutAuth,
}: {
  children?: any;
  tryLogin?: boolean;
  redirectOnFail?: boolean;
  renderWithoutAuth?: boolean;
}) => {
  const router = useRouter();

  const [authData, setAuthData] = useState(undefined as AuthData | undefined);
  const { signIn } = useGoogleLogin({
    ...loginProps,
    onSuccess: (res) => {
      // @ts-ignore
      const accessToken = res.accessToken;
      // @ts-ignore
      const profileObj = res.profileObj;

      setAuthData({
        accessToken,
        profile: {
          firstName: profileObj.givenName,
          fullName: profileObj.name,
          email: profileObj.email,
          imageUrl: profileObj.imageUrl,
        },
      });
    },
    onFailure: () => redirectOnFail && router.push(""),
    isSignedIn: !!tryLogin,
  });

  const { signOut } = useGoogleLogout(loginProps);

  useEffect(() => {
    if (!authData && tryLogin) {
      router.push("");
    }
  }, [authData]);

  return (
    <AuthContext.Provider
      value={{
        isLoggedIn: !!authData,
        login: signIn,
        logout: () => {
          signOut();
          setAuthData(undefined);
        },
        authData,
      }}
    >
      {(!!authData || renderWithoutAuth) && children}
    </AuthContext.Provider>
  );
};
