import { createContext, useContext, useState } from "react";

export type TSheetID = {
  sheetId: string;
  worksheetId: string;
};

export type TSheetContext = {
  sheetId: TSheetID;
  setSheet: (sheet: TSheetID) => void;
  resetSheet: () => void;
};

const DEFAULT_SHEET_ID = process.env.NEXT_PUBLIC_DEFAULT_SHEET_ID || "";
const DEFAULT_WORKSHEET_ID = process.env.NEXT_PUBLIC_DEFAULT_WORKSHEET_ID || "";

const defaultSheetID = {
  sheetId: DEFAULT_SHEET_ID,
  worksheetId: DEFAULT_WORKSHEET_ID,
};

export const SheetContext = createContext<TSheetContext>({
  sheetId: defaultSheetID,
  setSheet: () => {},
  resetSheet: () => {},
});

export const SheetProvider = ({ children }) => {
  const [sheetId, setSheet] = useState<TSheetID>(defaultSheetID);
  const resetSheet = () => setSheet(defaultSheetID);

  return (
    <SheetContext.Provider value={{ sheetId, setSheet, resetSheet }}>
      {children}
    </SheetContext.Provider>
  );
};

export const useSheetContext = () => {
  return useContext(SheetContext);
};
