import axios from "axios";
import { useAuthData } from "../context/AuthContext";
import { useSheetContext } from "../context/SheetContext";

export const useSheetGetterFromContext = () => {
  const { isLoggedIn, authData } = useAuthData();
  const {
    sheetId: { sheetId, worksheetId },
  } = useSheetContext();

  if (!isLoggedIn || !authData) throw new Error("No auth data found");

  return () =>
    axios
      .get(
        `https://content-sheets.googleapis.com/v4/spreadsheets/${sheetId}/values/${worksheetId}!A:ZA`,
        {
          headers: {
            Authorization: `Bearer ${authData.accessToken}`,
            "content-type": "application/json",
          },
        }
      )
      .then((res) => res.data)
      .then((data) => {
        const [header, ...rows] = data.values;
        return rows.map((row: any) =>
          row.reduce(
            (acc: { [x: string]: any }, value: any, index: string | number) => {
              acc[header[index]] = value;
              return acc;
            },
            {}
          )
        );
      });
};
