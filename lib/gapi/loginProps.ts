export const loginProps = {
  clientId: process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID || "",
  scope: "profile email https://www.googleapis.com/auth/spreadsheets",
  isSignedIn: true,
};
