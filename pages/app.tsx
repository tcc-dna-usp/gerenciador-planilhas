import { AuthProvider } from "../lib/context/AuthContext";
import { Box } from "@mui/system";
import { CustomAppBar } from "../components/CustomAppBar";
import Head from "next/head";
import { SheetViewerHoc } from "../components/SheetViewerHoc";

export default function App() {
  return (
    <AuthProvider tryLogin={true}>
      <Head>
        <title>G-(DNA USP) - Home</title>
      </Head>
      <Box>
        <CustomAppBar />
        <Box>
          <SheetViewerHoc />
        </Box>
      </Box>
    </AuthProvider>
  );
}
