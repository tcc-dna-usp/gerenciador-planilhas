import { AuthProvider } from "../lib/context/AuthContext";
import { Box } from "@mui/system";
import { CustomAppBar } from "../components/CustomAppBar";
import Head from "next/head";

export default function Home() {
  return (
    <AuthProvider tryLogin={true} renderWithoutAuth>
      <Head>
        <title>G-(DNA USP) - Home</title>
      </Head>
      <Box sx={{ display: "flex" }}>
        <CustomAppBar />
      </Box>
    </AuthProvider>
  );
}
